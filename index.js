'use strict';

var fs      = require('fs');
var path    = require('path');
var mime    = require('mime');
var gutil   = require('gulp-util');
var through = require('through2');


module.exports = function() {
  
  function getMimeType(value) {
    var orig = value.split('/');
    var ret = {};
    switch (orig[1]) {
      case 'x-font-ttf':
        ret.type = 'application';
        ret.subtype = 'x-font-ttf';
        ret.format = 'truetype';
        break;
      case 'font-woff':
        ret.type = 'font';
        ret.subtype = 'woff';
        ret.format = 'woff';
        break;
      default:
        ret.type = orig[0];
        ret.subtype = orig[1];
        ret.format = orig[1];
        break;
    }
    return ret;
  };
  
  function gulpSimpleFont64(file, enc, callback) {

    // Do nothing if no contents
    if (file.isNull()) {
      this.push(file);
      return callback();
    }

    if (file.isStream()) {
      this.emit('error', new gutil.PluginError("gulp-simplefont64", "Stream content is not supported"));
      return callback();
    }

    if (file.isBuffer()) {
      var fontToBase64 = new Buffer(file.contents).toString('base64'),
          fileName = path.basename(file.path, path.extname(file.path)),
          mimeType = getMimeType(mime.lookup(file.path)),
          styleRules = {
            black:      "font-weight: 800;",
            extrabold:  "font-weight: 800;",
            bold:       "font-weight: 700;",
            demibold:   "font-weight: 600;",
            semibold:   "font-weight: 600;",
            regular:    "font-weight: 400;",
            light:      "font-weight: 200;",
            ultralight: "font-weight: 200;",
            italic:     "font-style: italic;"
          },

          // Filenames should be of the style: FontFamily-Style1-Style2...
          fontAttrs = fileName.split('-'),
          fontFamily = fontAttrs.shift(),
          css = '@font-face { font-family: ' + fontFamily + '; ';

      css += fontAttrs.map(function(attr) {
        // Format our font attributes
        return attr.toLowerCase();  
      }).reduce(function(prev, attr) {
        return styleRules[attr] ? prev + ' ' + styleRules[attr] : prev;
      }, String());
      css += 'src: url(\'data:' + mimeType.type +'/'+ mimeType.subtype + '; charset=utf-8; base64,' + fontToBase64 + '\') format(\''+mimeType.format+'\');}';

      file.contents = new Buffer(css);
      file.path = gutil.replaceExtension(file.path, '.'+ mime.lookup(file.path).split('/')[1] +'.css');

      return callback(null, file);
    }
  };

  return through.obj(gulpSimpleFont64);
};